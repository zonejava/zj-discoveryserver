FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/discovery-server.jar discovery-server.jar
ENTRYPOINT ["java","-jar","discovery-server.jar"]